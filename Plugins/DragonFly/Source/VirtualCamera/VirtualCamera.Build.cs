// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
using Path = System.IO.Path;
using UnrealBuildTool;

public class VirtualCamera : ModuleRules
{
	public VirtualCamera(ReadOnlyTargetRules Target) : base(Target)
    {
        PrivatePCHHeaderFile = "Private/VirtualCameraPrivatePCH.h";
        bUsePrecompiled = true; // this is set to true as a post-build step by PackagePluginForRelease.cmd

#if UE_4_24_OR_LATER
        DefaultBuildSettings = BuildSettingsVersion.V2;
        ShadowVariableWarningLevel = WarningLevel.Error;
#endif

        PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"VirtualCamera/Private/Slate",
                "VirtualCamera/Private/MaUI",
                "VirtualCamera/Private/MaUI/Styles",
                "VirtualCamera/Private/Bindings"
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
            {
                "Core", "CoreUObject", "Engine", "InputCore"
				// ... add other public dependencies that you statically link with here ...
			}
			);

        PrivateDependencyModuleNames.AddRange(
			new string[]
            {
                "RenderCore",
                "HeadMountedDisplay",
                "Projects",
				"InputCore",
				"UnrealEd",
				"LevelEditor",
                "Sequencer",
                "MovieScene",
                "MovieSceneTools",
                "SequenceRecorder",
                "CoreUObject",
				"Engine",
				"Settings",
				"Slate",
				"SlateCore",
                "CinematicCamera",
                "EditorStyle",
                "VirtualCameraRuntime",
                "MovieSceneTracks",
                "LevelSequence",
                "PropertyEditor",
                "EditorWidgets",
                "SequencerWidgets",
                "AssetRegistry",
                "AssetTools",//By ExportSystem for LevelSequence Asset creation!
				"KismetWidgets",
				"DesktopPlatform",
                "MovieSceneCapture",
                "Json",
                "JsonUtilities",
                "SessionServices",
                "ImageWrapper",
                "FBX",
                "BlueprintGraph"
                         
				// ... add private dependencies that you statically link with here ...	
			}
            );
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);

        // VirtualCameraCore

        string VCamCorePath = Path.GetFullPath(Path.Combine(ModuleDirectory, "..", "..", "VirtualCameraCore"));
        if ((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32))
        {
            string NatNetLibPath = Path.Combine(VCamCorePath, "VirtualCameraCore", "ThirdParty", "NatNetSDK", "lib",
                ((Target.Platform == UnrealTargetPlatform.Win64) ? "x64" : "x86"));

            RuntimeDependencies.Add(Path.Combine(NatNetLibPath, "NatNetLib.dll"));
            PublicDelayLoadDLLs.Add("NatNetLib.dll");

            string CoreLibPath = Path.Combine(VCamCorePath, 
                ((Target.Platform == UnrealTargetPlatform.Win64) ? "x64" : "x86"), 
                (Target.Configuration == UnrealTargetConfiguration.Debug && Target.bDebugBuildsActuallyUseDebugCRT) ? "Debug" : "Release");

            PublicAdditionalLibraries.Add(Path.Combine(CoreLibPath, "VirtualCameraCore.lib"));
            PublicDelayLoadDLLs.Add("VirtualCameraCore.dll");
            RuntimeDependencies.Add(Path.Combine(CoreLibPath, "VirtualCameraCore.dll"));
            PublicIncludePaths.Add(Path.Combine(VCamCorePath, "VirtualCameraCore"));
        }
    }
}
