# Technocrane Unreal Engine Plugin

[Technocrane Tracker Plugin on an official marketplace](https://unrealengine.com/marketplace/en-US/slug/technocrane-tracker?fbclid=IwAR0Fj0Ma3GsJ5hbG-oYZzfJeObZsdd8iSZKVrDtZPjCpr2IUWdOS83wVVsA)

# How to Install Manually

  Create a "Plugins" directory in the project root (near the "Content" directory) and create there sub-folder "TechnocranePlugin". Put the repository folders into the "TechnocranePlugin" folder. (e.g. "/MyProject/Plugins/TechnocranePlugin")

  When you are opening project for a first time, you have to go to Settings->Plugins, find there Technocrane Plugin and Enable it.
[![TechnocranePlugin](https://github.com/technocranes/technocrane-unreal/blob/master/Images/TechnocranePlugin.JPG)]()

# Video Tutorial

[![plugin_introduction](https://youtu.be/Nxp08jvDGdk)](https://youtu.be/Nxp08jvDGdk)

# Contact

Please post issues and feature requests to this [github repository issues section](https://github.com/technocranes/technocrane-unreal/issues)
